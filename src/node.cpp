#include "node.h"
#include <cmath>
#include <exception>
#include <ctime>
namespace kann {

	node::node(size_t inputs) {
		
		std::srand(std::time(nullptr));

		this->m_inputs.resize(inputs);
		this->threshold = this->_random();

		auto it = this->m_inputs.begin();
		auto itEnd = this->m_inputs.end();

		while (it != itEnd) {
			it->m_weight = this->_random();
			it->m_input = 0;

			++it;
		}

	}

	float node::_random() const {
		float val = static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX);
		float sign = static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX);
		if (sign < 0.5) {
			return val * -1;
		}

		return val;
	}

	float node::training_speed() const {
		return this->trainingWeight;
	}

	void node::training_speed(float training_speed) {
		this->trainingWeight = training_speed;
	}

	float node::_calculate() const {

		auto it = this->m_inputs.begin();
		auto itEnd = this->m_inputs.end();

		float sum = 0.0f;

		while (it != itEnd) {
			sum += it->m_input * it->m_weight;
			++it;
		}

		return sum;
	}

	float node::result() const {
		if (this->_calculate() >= this->threshold) {
			return 1.0f;
		}
		return 0.0f;
	}

	void node::input(size_t node, float value) {
		if (node >= this->m_inputs.size()) {
			throw new std::exception("Node does not exist!");
		}

		this->m_inputs[node].m_input = value;
	}

	void node::train(float output) {
		float result = this->result();
		if (result == output) {
			return;
		}

		this->_updateWeights(result, output);
	}

	void node::_updateWeights(float current, float target) {

		auto it = this->m_inputs.begin();
		auto itEnd = this->m_inputs.end();

		while (it != itEnd) {
			it->m_weight += this->trainingWeight * (target - current) * it->m_input;
			++it;
		}

		this->threshold -= this->trainingWeight * (target - current);

	}

	node::~node() {

	}
}