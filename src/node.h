#include <vector>
#include <memory>

namespace kann {
	class node {
		private:

			struct dendrite {
				float m_input;							//INPUT VALUE
				float m_weight;							//CURRENT NODE WEIGHT
			};

			std::vector<dendrite> m_inputs;				//INPUTS

			float threshold;
			float trainingWeight;
			float last_output;
			bool trigger;
		protected:
			float _calculate() const;
			void _updateWeights(float current, float target);
			float _random() const;
		public:
			node(size_t);

			void training_speed(float training_speed);
			float training_speed() const;

			void input(size_t input, float value);
			float result() const;
			float last_result() const;				//RETURNS THE LAST RESULT
			void train(float output);

			~node();
	};

	typedef std::shared_ptr<node> node_ptr;
}