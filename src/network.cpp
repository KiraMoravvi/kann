#include "network.h"
#include <list>

namespace kann {
	network::network() {

	}

	void network::reserve_nodes(size_t count) {
		nodes.reserve(count);
	}


	void network::input(const std::vector<float>& inputs) {
		if (inputs.size() == this->inputList.size()) {
			for (size_t i = 0; i < inputs.size(); ++i) {
				this->inputList.at(i)->input(0, inputs.at(i));
			}
		}
	}

	size_t network::count_inputs() const {
		return this->inputList.size();
	}

	//THESE ARE EFFECTIVELY INPUT NODES
	std::vector<node_ptr> network::add_nodes(size_t count) {
		std::vector<node_ptr> addedNodes(count);

		for (size_t i = 0; i < count; ++i) {
			node_ptr newNode = std::make_shared<node>(1);
			addedNodes[i] = newNode;
			this->nodes.push_back(newNode);
			this->inputList.push_back(newNode.get());
			this->nextAvailableDendrite[newNode.get()] = 0;
			//ADDS AN ENTRY TO THE NODE
			this->ouputTo[newNode.get()];
		}

		return addedNodes;
		
	}

	std::vector<node_ptr> network::add_nodes(size_t count, const std::vector<node_ptr>& parentNodes) {
		if (parentNodes.size() == 0) {
			return this->add_nodes(count);
		}
		std::vector<node_ptr> addedNodes(count);

		for (size_t i = 0; i < count; ++i) {
			node_ptr newNode = std::make_shared<node>(parentNodes.size());
			addedNodes[i] = newNode;
			this->nodes.push_back(newNode);
			this->ouputTo[newNode.get()];
			this->nextAvailableDendrite[newNode.get()] = 0;
		}

		//LOOP THROUGH THE NODES CHOSEN AND PARENT NODES AND ADD THE NODES ADDED AS CHILD NODES
		auto it = parentNodes.begin();
		while (it != parentNodes.end()) {


			//FIND THE PARENT NODE
			auto itOutputTo = this->ouputTo.find(it->get());
			if (itOutputTo != this->ouputTo.end()) {
				//ADD THE ADDED NODES TO THE PARENT NODES
				auto itAddedNodes = addedNodes.begin();
				while (itAddedNodes != addedNodes.end()) {
					nodeLink tmp;

					tmp.targetNode = itAddedNodes->get();
					tmp.targetDendrite = this->nextAvailableDendrite[itAddedNodes->get()]++;
					
					itOutputTo->second.push_back(tmp);
					++itAddedNodes;
				}
			}
			++it;
		}

		return addedNodes;
	}

	void network::step(size_t steps) {
		//BREADTH FIRST ALGORITHM
		std::map<node*, std::vector<node*>> toPropogate;
		std::map<node*, bool> checkedNode;
		std::list<node*> nodesToCheck(this->inputList.begin(), this->inputList.end());

		auto checkIt = nodesToCheck.begin();

		while (checkIt != nodesToCheck.end()) {
			if (checkedNode.find(*checkIt) == checkedNode.end()) {
				checkedNode[*checkIt] = true;

				//LOOP THROUGH THE OUTPUTS AND APPEND THE CHILDREN TO THE LIST OF NODES TO CHECK
				std::vector<nodeLink>* pNodeList = &this->ouputTo.at(*checkIt);
				auto itOutput = pNodeList->begin();
				while (itOutput != pNodeList->end()) {
					if (checkedNode.find(itOutput->targetNode) == checkedNode.end()) {
						checkedNode[*checkIt] = true;
						nodesToCheck.push_back(itOutput->targetNode);
					}
					++itOutput;
				}
			}
			
			++checkIt;
		}

		checkIt = nodesToCheck.begin();

		//LOOP THROUGH ALL THE NODES THAT NEED TO BE CHECKED
		while (checkIt != nodesToCheck.end()) {

			//GET THE LIST OF OUTPUT NODES
			std::vector<nodeLink>* pOutputTo = &this->ouputTo.at(*checkIt);

			//LOOP THROUGH THE LIST OF CURRENT NODE TO CHECK NODE OUTPUT NODES
			auto outputIt = pOutputTo->begin();
			auto outputItEnd = pOutputTo->end();
			while (outputIt != outputItEnd) {
				nodeLink* curOutputNode = &(*outputIt);
				curOutputNode->targetNode->input(curOutputNode->targetDendrite, (*checkIt)->result());
				++outputIt;
			}
			
			++checkIt;
		}
	}

	void network::teach(const std::vector<float>& output) {

	}

	network::~network() {

	}
}