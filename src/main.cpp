#include "network.h"
#include <iostream>

int main() {
	/*
	kann::node perceptron(2);

	perceptron.training_speed(5.5f);

	float trainingInput1[10] = {  100.0f,  0.0f };
	float trainingInput2[10] = {  0.0f,  100.0f };

	float trainingOutput[10] = { 5.0f, -5.0f };
	
	for (size_t i = 0; i < 2; ++i) {
		perceptron.input(0, trainingInput1[i]);
		perceptron.input(1, trainingInput2[i]);


		perceptron.train(trainingOutput[i]);
		std::cout << i << ": " << perceptron.result() << std::endl;

	}
	perceptron.input(0, 15);
	perceptron.input(1, 99);

	std::cout << "Result: " << perceptron.result() << std::endl;
	*/

	kann::network myNetwork;

	//NETWORK TO TRAIN AN XOR GATE
	auto inNodes = myNetwork.add_nodes(2);
	auto hiddenNodes = myNetwork.add_nodes(2, inNodes);
	auto outNodes = myNetwork.add_nodes(1, hiddenNodes);

	myNetwork.input({ 0.0f, 1.0f });
	myNetwork.teach({ 1.0f });

	myNetwork.input({ 1.0f, 1.0f });
	myNetwork.teach({ 1.0f });

	myNetwork.input({ 1.0f, 0.0f });
	myNetwork.teach({ 1.0f });

	myNetwork.input({ 0.0f, 0.0f });
	myNetwork.teach({ 0.0f });


	//myNetwork.step();

	std::cout << "Result: " << std::endl;
	int x = 0;
	auto it = outNodes.begin();
	while (it != outNodes.end()) {
		std::cout << "Output " << x << ": " << (*it)->result() << std::endl;
		++it;
	}
	system("PAUSE");
}