#include <vector>
#include <map>

#include "node.h"
namespace kann {
	class network {
		private:
			struct nodeLink {
				node* targetNode;
				unsigned int targetDendrite;
			};
		protected:
			std::vector<node_ptr> nodes;
			std::map<node*, std::vector<nodeLink>> ouputTo;
			std::map<node*, unsigned int> nextAvailableDendrite;
			std::vector<node*> inputList;
		public:
			network();
			
			void reserve_nodes(size_t nodes);

			size_t count_inputs() const;
			void input(const std::vector<float>& inputs);

			const std::vector<node_ptr>& input_nodes() const;

			std::vector<node_ptr> add_nodes(size_t nodes);
			std::vector<node_ptr> add_nodes(size_t nodes, const std::vector<node_ptr>& parents);

			void step(size_t steps = 1);
			void step_until_output();

			void teach(const std::vector<float>& output);

			~network();
	};
}